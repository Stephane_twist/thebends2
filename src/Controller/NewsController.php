<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends AbstractController
{
    /**
     * @Route("/news")
     */
    public function getNews()
    {
        $news = [];
        return $this->render('news/index.html.twig',[
            'news' => $news,
        ]);
    }

    /**
     * @Route("/news/{articleName}")
     */
    public function getArticle($articleName)
    {
        return $this->render('news/news.html.twig',[
            'title' => ucwords($articleName,'-'),
        ]);
    }
}