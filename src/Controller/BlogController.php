<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog")
     */
    public function getBlog()
    {
        $blogEntries = [];
        return $this->render('blog/index.html.twig',[
            'blog' => $blogEntries,
        ]);
    }

    /**
     * @Route("/blog/{articleName}")
     */
    public function getArticle($articleName)
    {
        return $this->render('blog/blog.html.twig',[
            'title' => ucwords($articleName,'-'),
        ]);
    }
}