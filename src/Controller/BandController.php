<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Band;

class BandController extends AbstractController
{
    /**
     *
     * @Route("/bands")
     *
     */
    public function getBandList()
    {
        return $this->render('band/bands.html.twig',[
        ]);
    }

    /**
     *
     * @Route("/bands/{bandName}")
     *
     * @param $bandName
     * @return
     */
    public function getBand($bandName)
    {
        return $this->render('band/band.html.twig',[
            'title' => $bandName,
            'band' => $bandName,
        ]);
    }
}
