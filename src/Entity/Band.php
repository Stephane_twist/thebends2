<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BandRepository")
 */
class Band
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=127)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $yearFounded;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getYearFounded(): ?\DateTimeInterface
    {
        return $this->yearFounded;
    }

    public function setYearFounded(?\DateTimeInterface $yearFounded): self
    {
        $this->yearFounded = $yearFounded;

        return $this;
    }
}
